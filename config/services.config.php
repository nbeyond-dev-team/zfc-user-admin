<?php
/**
 * User: Vladimir Garvardt
 * Date: 3/18/13
 * Time: 6:39 PM
 */
use Zend\ServiceManager\ServiceLocatorInterface;
use ZfcUser\Form\RegisterFilter;
use ZfcUser\Mapper\UserHydrator;
use ZfcUser\Validator\NoRecordExists;
use ZfcUserAdmin\Form;
use ZfcUserAdmin\Options;
use ZfcUserAdmin\Validator\NoRecordExistsEdit;

return array(
    'invokables' => array(
        'ZfcUserAdmin\Form\EditUser' => 'ZfcUserAdmin\Form\EditUser'
    ),
    'factories' => array(
        'zfcuser_user_mapper' => function (ServiceLocatorInterface $sm) {
            /** @var $config \ZfcUserAdmin\Options\ModuleOptions */
            $config = $sm->get('zfcuseradmin_module_options');
            $mapperClass = $config->getUserMapper();
            if (stripos($mapperClass, 'doctrine') !== false) {
                $mapper = new $mapperClass(
                    $sm->get('zfcuser_doctrine_em'),
                    $sm->get('zfcuser_module_options')
                );
            } else {
                /** @var $zfcUserOptions \ZfcUser\Options\UserServiceOptionsInterface */
                $zfcUserOptions = $sm->get('zfcuser_module_options');

                /** @var $mapper \ZfcUserAdmin\Mapper\UserZendDb */
                $mapper = new $mapperClass();
                $mapper->setDbAdapter($sm->get('zfcuser_zend_db_adapter'));
                $entityClass = $zfcUserOptions->getUserEntityClass();
                $mapper->setEntityPrototype(new $entityClass);
                $mapper->setHydrator($sm->get('zfcuser_user_hydrator'));
                $mapper->setTableName($zfcUserOptions->getTableName());
            }

            return $mapper;
        },
    ),
);
