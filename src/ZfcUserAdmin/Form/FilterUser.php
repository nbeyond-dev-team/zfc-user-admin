<?php


namespace ZfcUserAdmin\Form;


use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class FilterUser extends Form
{
    public function __construct()
    {
        parent::__construct('filter-user');

        $this->setAttribute('method', 'get');
        $this->addFields();
    }

    private function addFields()
    {
        $this->add([
            'type' => Text::class,
            'name' => 'id',
            'options' => [
                'label' => 'ID',
                'label_attributes' => [
                    'class' => 'col-sm-2'
                ],
                'column-size' => 'sm-2',
            ],
            'attributes' => [
                'class' => 'form-control',
                'autocomplete' => 'off'
            ],
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'email',
            'options' => [
                'label' => 'Email',
                'label_attributes' => [
                    'class' => 'col-sm-2'
                ],
                'column-size' => 'sm-2',
            ],
            'attributes' => [
                'class' => 'form-control',
                'autocomplete' => 'off'
            ],
        ]);

        $this->add([
            'type' => Submit::class,
            'name' => 'filter-user',
            'attributes' => [
                'class' => 'btn btn-primary col-sm-2',
                'style' => 'margin-top: 1.8%;',
                'value' => 'Filter'
            ],
        ]);
    }
}