<?php

namespace ZfcUserAdmin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator;
use Zend\Hydrator\ClassMethods;
use ZfcUser\Mapper\UserInterface;
use ZfcUser\Options\ModuleOptions as ZfcUserModuleOptions;
use ZfcUserAdmin\Form\CreateUser;
use ZfcUserAdmin\Form\EditUser;
use ZfcUserAdmin\Form\FilterUser;
use ZfcUserAdmin\Options\ModuleOptions;
use ZfcUserAdmin\Service\User;

class UserAdminController extends AbstractActionController
{
    /** @var ModuleOptions */
    protected $options;
    /** @var \MyUser\Mapper\User */
    protected $userMapper;
    /** @var \ZfcUser\Options\UserServiceOptionsInterface */
    protected $zfcUserOptions;
    /** @var User */
    protected $adminUserService;
    /** @var CreateUser */
    protected $createUserForm;
    /** @var EditUser */
    protected $editUserForm;
    /** @var FilterUser */
    protected $filterUserForm;

    public function __construct($options, $userMapper, $adminUserService, $zfcUserOptions, $createUserForm, $editUserForm, $filterUserForm)
    {
        $this->options = $options;
        $this->userMapper = $userMapper;
        $this->adminUserService = $adminUserService;
        $this->zfcUserOptions = $zfcUserOptions;
        $this->createUserForm = $createUserForm;
        $this->editUserForm = $editUserForm;
        $this->filterUserForm = $filterUserForm;
    }

    public function listAction()
    {
        $params = $this->params()->fromQuery();
        $this->filterUserForm->setData($params);
        $userMapper = $this->userMapper;
        $users = $userMapper->findByFilters($params);
        if (is_array($users)) {
            $paginator = new Paginator\Paginator(new Paginator\Adapter\ArrayAdapter($users));
        } else {
            $paginator = $users;
        }

        $paginator->setItemCountPerPage(100);
        $paginator->setCurrentPageNumber($this->getEvent()->getRouteMatch()->getParam('p'));
        return array(
            'users' => $paginator,
            'userlistElements' => $this->options->getUserListElements(),
            'filterUserForm' => $this->filterUserForm
        );
    }

    public function createAction()
    {
        $request = $this->getRequest();

        /** @var $request \Zend\Http\Request */
        if ($request->isPost()) {
            $zfcUserOptions = $this->zfcUserOptions;
            $class = $zfcUserOptions->getUserEntityClass();
            $user = new $class();
            $this->createUserForm->setHydrator(new ClassMethods());
            $this->createUserForm->bind($user);
            $this->createUserForm->setData($request->getPost());

            if ($this->createUserForm->isValid()) {
                $user = $this->adminUserService->create($this->createUserForm, (array)$request->getPost());
                if ($user) {
                    $this->flashMessenger()->addSuccessMessage('The user was created');
                    return $this->redirect()->toRoute('zfcadmin/zfcuseradmin/list');
                }
            }
        }

        return array(
            'createUserForm' => $this->createUserForm
        );
    }

    public function editAction()
    {
        $userId = $this->getEvent()->getRouteMatch()->getParam('userId');
        $user = $this->userMapper->findById($userId);
        
        $this->editUserForm->setUser($user);

        /** @var $request \Zend\Http\Request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->editUserForm->setData($request->getPost());
            if ($this->editUserForm->isValid()) {
                $user = $this->adminUserService->edit($this->editUserForm, (array)$request->getPost(), $user);
                if ($user) {
                    $this->flashMessenger()->addSuccessMessage('The user was edited');
                    return $this->redirect()->toRoute('zfcadmin/zfcuseradmin/list');
                }
            }
        } else {
            $this->editUserForm->populateFromUser($user);
        }

        return array(
            'editUserForm' => $this->editUserForm,
            'userId' => $userId
        );
    }

    public function removeAction()
    {
        $userId = $this->getEvent()->getRouteMatch()->getParam('userId');

        /** @var $identity \ZfcUser\Entity\UserInterface */
        $identity = $this->zfcUserAuthentication()->getIdentity();
        if ($identity && $identity->getId() == $userId) {
            $this->flashMessenger()->addErrorMessage('You can not delete yourself');
        } else {
            $user = $this->userMapper->findById($userId);
            if ($user) {
                $this->userMapper->remove($user);
                $this->flashMessenger()->addSuccessMessage('The user was deleted');
            }
        }

        return $this->redirect()->toRoute('zfcadmin/zfcuseradmin/list');
    }
}
