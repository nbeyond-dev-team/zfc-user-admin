<?php


namespace ZfcUserAdmin\Factory\Form;


use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ZfcUserAdmin\Form\FilterUser;

class FilterUserFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new FilterUser();
    }
}