<?php
namespace ZfcUserAdmin\Factory\Form;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ZfcUser\Form\RegisterFilter;
use ZfcUser\Validator\NoRecordExists;
use ZfcUserAdmin\Form\CreateUser;

class CreateUserFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var $zfcUserOptions \ZfcUser\Options\UserServiceOptionsInterface */
        $zfcUserOptions = $container->get('zfcuser_module_options');
        /** @var $zfcUserAdminOptions \ZfcUserAdmin\Options\ModuleOptions */
        $zfcUserAdminOptions = $container->get('zfcuseradmin_module_options');
        $form = new CreateUser(null, $zfcUserAdminOptions, $zfcUserOptions);
        $filter = new RegisterFilter(
            new NoRecordExists(array(
                'mapper' => $container->get('zfcuser_user_mapper'),
                'key' => 'email'
            )),
            new NoRecordExists(array(
                'mapper' => $container->get('zfcuser_user_mapper'),
                'key' => 'username'
            )),
            $zfcUserOptions
        );
        if ($zfcUserAdminOptions->getCreateUserAutoPassword()) {
            $filter->remove('password')->remove('passwordVerify');
        }
        $form->setInputFilter($filter);
        return $form;
    }
}