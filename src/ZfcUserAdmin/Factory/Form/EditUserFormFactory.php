<?php
namespace ZfcUserAdmin\Factory\Form;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ZfcUser\Form\RegisterFilter;
use ZfcUserAdmin\Form\EditUser;
use ZfcUserAdmin\Validator\NoRecordExistsEdit;

class EditUserFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var $zfcUserOptions \ZfcUser\Options\UserServiceOptionsInterface */
        $zfcUserOptions = $container->get('zfcuser_module_options');
        /** @var $zfcUserAdminOptions \ZfcUserAdmin\Options\ModuleOptions */
        $zfcUserAdminOptions = $container->get('zfcuseradmin_module_options');
        $form = new EditUser(null, $zfcUserAdminOptions, $zfcUserOptions);
        $filter = new RegisterFilter(
            new NoRecordExistsEdit(array(
                'mapper' => $container->get('zfcuser_user_mapper'),
                'key' => 'email'
            )),
            new NoRecordExistsEdit(array(
                'mapper' => $container->get('zfcuser_user_mapper'),
                'key' => 'username'
            )),
            $zfcUserOptions
        );
        if (!$zfcUserAdminOptions->getAllowPasswordChange()) {
            $filter->remove('password')->remove('passwordVerify');
        } else {
            $filter->get('password')->setRequired(false);
            $filter->remove('passwordVerify');
        }
        $form->setInputFilter($filter);
        return $form;
    }
}