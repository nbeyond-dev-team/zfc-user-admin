<?php
namespace ZfcUserAdmin\Factory\Controller;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ZfcUserAdmin\Controller\UserAdminController;


class UserAdminControllerFactory implements FactoryInterface
{
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new UserAdminController(
            $container->get('zfcuseradmin_module_options'),
            $container->get('zfcuser_user_mapper'),
            $container->get('zfcuseradmin_user_service'),
            $container->get('zfcuser_module_options'),
            $container->get('zfcuseradmin_createuser_form'),
            $container->get('zfcuseradmin_edituser_form'),
            $container->get('zfcuseradmin_filteruser_form')
        );
    }

}