<?php
namespace ZfcUserAdmin\Factory\Service;

class UserServiceFactory implements \Zend\ServiceManager\Factory\FactoryInterface
{
    public function __invoke(\Interop\Container\ContainerInterface $container, $requestedName, array $options = null)
    {
        return new \ZfcUserAdmin\Service\User(
            $container->get('zfcuser_user_mapper'),
            $container->get('zfcuseradmin_module_options'),
            $container->get('zfcuser_module_options')
        );
    }
}